﻿using System;

/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */

namespace GamesHub
{
	public class Blackjack : IGameCommands
	{
		private static string[] playerCards = new string[11];
		private static string hitOrStay = "";
		private static int total = 0, count = 1, dealerTotal = 0;
		private static Random cardRandomizer = new Random();

		public Blackjack(){}

		public void Start()
		{
			Console.Title = "Seth's Blackjack!";
			dealerTotal = cardRandomizer.Next(15, 22);
			playerCards[0] = Deal();
			playerCards[1] = Deal();
			do
			{
				Console.WriteLine("Welcome to Blackjack! You were dealt " + playerCards[0] + " and " + playerCards[1] + ". \nYour total is " + total + ".\nWould you like to hit or stay?");
				hitOrStay = Console.ReadLine().ToLower();
			} while (!hitOrStay.Equals("hit") && !hitOrStay.Equals("stay"));
			Game();
		}

		private void Game()
		{
			if (hitOrStay.Equals("hit"))
			{
				Hit();
			}
			else if (hitOrStay.Equals("stay"))
			{
				if (total > dealerTotal && total <= 21)
				{
					Console.WriteLine("\nCongrats! You won the game! The dealer's total was " + dealerTotal + ".\nWould you like to play again? y/n");
				}
				else if (total < dealerTotal)
				{
					Console.WriteLine("\nSorry, you lost! The dealer's total was " + dealerTotal + ".\nWould you like to play again? y/n");
				}
			}
			Console.ReadLine();
		}
		private string Deal()
		{
			string Card = "";
			int cards = cardRandomizer.Next(1, 14);
			switch (cards)
			{
			case 1: Card = "Two"; total += 2;
				break;
			case 2: Card = "Three"; total += 3;
				break;
			case 3: Card = "Four"; total += 4;
				break;
			case 4: Card = "Five"; total += 5;
				break;
			case 5: Card = "Six"; total += 6;
				break;
			case 6: Card = "Seven"; total += 7;
				break;
			case 7: Card = "Eight"; total += 8;
				break;
			case 8: Card = "Nine"; total += 9;
				break;
			case 9: Card = "Ten"; total += 10;
				break;
			case 10: Card = "Jack"; total += 10;
				break;
			case 11: Card = "Queen"; total += 10;
				break;
			case 12: Card = "King"; total += 10;
				break;
			case 13: Card = "Ace"; total += 11;
				break;
			default: Card = "2"; total += 2;
				break;
			}
			return Card;
		}

		private void Hit()
		{
			count += 1;
			playerCards[count] = Deal();
			Console.WriteLine("\nYou were dealt a(n) " + playerCards[count] + ".\nYour new total is " + total + ".");
			if (total.Equals(21))
			{
				Console.WriteLine("\nYou got Blackjack! The dealer's total was " + dealerTotal + ".\nWould you like to play again?");
			}
			else if (total > 21)
			{
				Console.WriteLine("\nYou busted, therefore you lost. Sorry. The dealer's total was " + dealerTotal + ".\nWould you like to play again? y/n");
			}
			else if (total < 21)
			{
				do
				{
					Console.WriteLine("\nWould you like to hit or stay?");
					hitOrStay = Console.ReadLine().ToLower();
				} while (!hitOrStay.Equals("hit") && !hitOrStay.Equals("stay"));
				Game();
			}
		}

		public  void Help(){
			Console.WriteLine ("Insert instructions here");
		}
		public void Quit() {
			//TODO needs to be implemented
		}
		public void Options() {
			//prints the available actions the user can take
		}
	}
}

