﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */

namespace GamesHub
{
   public class Ship
   {
      private string name;
      private int size;
      private List<int> coordinates = new List<int>();
      private int[,] sea = new int[10, 10];

      public Ship (string name, int size)
      {
         this.name = name;
         this.size = size;

         int counter = 0;
         for (int row = 0; row < 10; row++) {
            for (int column = 0; column < 10; column++) {
               this.sea [row, column] = counter;
               counter++;
            }
         }
      }

      public string GetName ()
      {
         return name;
      }

      public int GetSize ()
      {
         return size;
      }

      public int GetSea(int column, int row)
      {
         return sea[column, row];
      }

      public List<int> GetCoords()
      {
         return coordinates;
      }


      //A function that prompts the user to enter a coordinate seed.
      //The coordinate seed is then checked for validity. Depending on
      //the seed, the user may be prompted to choose horizontal or vertical
      //orientation. More coordinates are generated based on the size of the ship.
      //After all coordinates are generated, they are checked against a list of spots
      //already occupied by other ships.

      public List<int> CoordinatesGenerator (List<int> occupiedSpots)
      {
         SetAnchor: //Starting point used as a reference to start over if needed

         int[] ship = new int[size];
         string coords = UI.PromptLine (string.Format ("Set coordinates for your {0}: ", name));

         //A loop that checks if the coordinate entered is valid.
         while (!CoordsCheck (coords.ToLower())) {

            Console.WriteLine("Invalid coordinates. Try Again.");
            coords = UI.PromptLine (string.Format ("Set coordinates for your {0}: ", name));
         }

         coords = coords.ToLower ();
         int x = (int)coords[0] - 97;
         int y = (int)coords[1] - 48;
         ship [0]  = sea [x, y]; //set anchor 
         int column = ship [0] % 10;
         int row = ship [0] / 10;
         int z = 10 - size;

         //Checks for valid ship placement. Some placements have mandatory orientations.
         //If orientation is variable, prompts user for more input.
         if (ship [0] == 99) {
            goto SetAnchor;
         } else if (column > z && row <= z) {
            ship = SetVertical(ship);
         } else if (column <= z && row > z) {
            ship= SetHorizontal(ship);
         } else if (column > z && row > z) {
            goto SetAnchor;
         } else {


            SetOrientation: //starting point for the user to select another orientation

            string orientation = UI.PromptLine("(H)orizontal or (V)ertical?: ");
            orientation = orientation.ToLower();

            //Takes user input and generates coordinates accordingly
            if (orientation[0]== 'h') {
               ship= SetHorizontal(ship);
            } else if (orientation[0] == 'v') { 
               ship = SetVertical(ship);
            }
            else {
               Console.Write ("Invalid answer. Try again.");
               goto SetOrientation; //GO TO LINE 93
            }
         }
         // Check if the coordinates has occupied spots in the occupiedSpots list. If so set shipTest to true to try again.
         foreach (int xy in ship){
            if (occupiedSpots.Contains (xy)){
               Console.WriteLine("Invalid Placement. Set Anchor again.");
               goto SetAnchor;
            }
         }

         //Add the ship coordinates(previously tested) to the occupiedSpots list. 
         for (int i = 0; i < ship.Length; i++) {
            occupiedSpots.Add (ship [i]);
         }


         foreach (int coord in ship) {
            coordinates.Add (coord);
         }
         return coordinates;
      }

      /// Set vertical coordinates for the ships.
      public static int[]  SetVertical (int[] ship)
      {
         for (int i = 1; i < ship.Length; i++) {
            ship [i] = ship [i - 1] + 10;
         }
         return ship;
      }

      /// Set horizontal coordinates for the ships.
      public static int[]  SetHorizontal (int[] ship)
      {
         for (int i = 1; i < ship.Length; i++) {
            ship [i] = ship [i - 1] + 1;
         }
         return ship;
      }

      //Checks user input for validity to be used in calculations
      static bool CoordsCheck (string coords)
      {
         if (coords.Length != 2) {
            return false;
         }
         else if ((int)coords[0] < 97 || (int)coords [0] > 106) {
            return false;
         }
         else if ((int)coords [1] < 48 || (int)coords [1] > 57) {
            return false;
         }
         else {
            return true;
         }
      }
   }
}

