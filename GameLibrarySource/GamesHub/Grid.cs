﻿using System;
using System.IO;
using System.Collections.Generic;

/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */

namespace GamesHub
{

   //This class manages the console display for the game.
   //It features a setup display for placing ships and an in-game
   //display for gameplay.

   public class Grid
   {
      private int[,] sea = new int[10, 10];
      private char[,] display = new char[10, 10];
      private char[,] setup = new char[10, 10];

      public Grid()
      {
         //initializes an array of integers to be referenced.
         int counter = 0;
         for (int row = 0; row < 10; row++) {               
            for (int column = 0; column < 10; column++) {
               this.sea [row, column] = counter;
               counter++;
            }
         }
         //Initializes a grid to be printed as a display
         //for the players during gameplay.
         for (int row = 0; row < 10; row++) {               
            for (int column = 0; column < 10; column++) {   
               this.display [row, column] = ' ';
            }
         }
         //Initializes a grid to be printed as a display
         // for the players to set their ships.
         for (int row = 0; row < 10; row++) {               
            for (int column = 0; column < 10; column++) {   
               this.setup [row, column] = ' ';
            }
         }
      }

      //Prints the display of hits, misses, and un-hits
      public void PrintDisplay(string player)               
      {
         Console.Clear ();
         Console.WriteLine ("                 <{0}>", player);
         Console.WriteLine ("     0   1   2   3   4   5   6   7   8   9");
         Console.WriteLine ("--------------------------------------------");
         for (int row = 0; row < 10; row++) {
            Console.Write (" {0} ", (char)(row + 65));
            for (int column = 0; column < 10; column++) {
               Console.Write (string.Format ("| {0} ", display [row, column]));
               }
            Console.Write ("|");
            Console.WriteLine ();
            Console.WriteLine ("--------------------------------------------");
            }

      }
        
      //prints a 2D array of ints 0-99. Debug purpose only
      public void PrintSea()                             
      {
         for (int row = 0; row < 10; row++) {
            for (int column = 0; column < 10; column++) {
               Console.Write (string.Format ("{0,4}", sea [row, column]));
            }
            Console.WriteLine ();
         }
         Console.WriteLine ();
      }

      ///Marks a miss in the display grid to be printed next time
      public void MakeMiss (int row, int column)      
      {
         display[row,column] = 'O';
         sea [row, column] = 0;
      }

      ////Marks a hit in the display grid to be printed next time
      public void MakeHit(int row, int column)        
      {
         display [row, column] = 'X';
         sea [row, column] = 0;
      }

      ///Prints the display for users to set their own ships pre-game
      public void PrintShips(string player)           
      {
         Console.Clear ();
         Console.WriteLine ("                 <{0}>", player);
         Console.WriteLine ("     0   1   2   3   4   5   6   7   8   9");
         Console.WriteLine ("--------------------------------------------");
         for (int row = 0; row < 10; row++) {
            Console.Write (" {0} ", (char)(row + 65));
            for (int column = 0; column < 10; column++) {
               Console.Write (string.Format ("| {0} ", setup [row, column]));
            }
            Console.Write ("|");
            Console.WriteLine ();
            Console.WriteLine ("--------------------------------------------");
         }

      }

      ///Marks a ship segment to be used in PrintShips
      public void MakeShip(int coord)                 
      {
         int column = coord % 10;
         int row = coord / 10;
         setup [row, column] = '#';

      }


   }
}