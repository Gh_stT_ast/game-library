﻿using System;

/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */

namespace GamesHub
{
	public interface IGameCommands
	{
		void Options();
		void Help();
		void Quit();
	}
}

