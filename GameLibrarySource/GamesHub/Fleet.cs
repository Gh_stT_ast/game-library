﻿using System;
using System.IO;
using System.Collections.Generic;

/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */

namespace GamesHub
{

   ///This class initializes Ship objects and prompts the user to supply coordinate
   ///starting points. It also maintains a list of the Ships.

   public class Fleet
   {
      private List<Ship> fleet = new List<Ship>();
      private List<int> occupiedCoordinates = new List<int>();


      public Fleet (string player, Grid owned)
      {
        
         Ship Carrier = new Ship ("Aircraft Carrier", 5);
         Ship Battleship = new Ship ("Battleship", 4);
         Ship Submarine = new Ship ("Submarine", 3);
         Ship Destroyer = new Ship ("Destroyer", 3);
         Ship PatrolBoat = new Ship ("PT Boat", 2);

         fleet = new List<Ship>(new[]{ Carrier, Battleship, Submarine, Destroyer, PatrolBoat });

         //A loop to prompt the user to enter a starting point for each of the ships
         foreach (Ship s in fleet) {
            owned.PrintShips (player);
            occupiedCoordinates.AddRange (s.CoordinatesGenerator (occupiedCoordinates));
            foreach (int coord in occupiedCoordinates) {
               owned.MakeShip (coord);
            }
            owned.PrintShips (player);
         }
         UI.PromptLine ("Press enter to continue...");

      }

      public List<Ship> GetFleet()
      {
         return fleet;
      }
   }
}