﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;

/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */

namespace GamesHub
{
	public class TwentyFour
	{
		public TwentyFour (){}

		List<string> signs = new List<string>(new[]{"*", "/", "+", "-"});

		public void Help(){
			Console.WriteLine ("Insert instructions here");
		}
		public void Quit() {
			//TODO needs to be implemented
		}
		public void Options() {
			//prints the available actions the user can take
		}



		static double Evaluate(string expression) {
			var loDataTable = new DataTable();
			var loDataColumn = new DataColumn("Eval", typeof (double), expression);
			loDataTable.Columns.Add(loDataColumn);
			loDataTable.Rows.Add(0);
			return (double) (loDataTable.Rows[0]["Eval"]);
		}

		public void Start () {
			int answer = UI.PromptInt("Press 1 to play or 2 for Help:");
			if (answer == 1) {
				Random r = new Random ();
				int first = r.Next (0, 10);
				int second = r.Next (0, 10);
				int third = r.Next (5, 10);
				int fourth = r.Next (5, 10);
				string[] uSigns = { "?", "?", "?" };

				int[] vars = { first, second, third, fourth };
				Console.WriteLine ("Fill in the blanks to make the equation equal to 24");
				Console.WriteLine ("Normal order of operations are followed");
				Console.WriteLine ();

				while (Array.IndexOf (uSigns, "?") != -1) {
					SignCheck (vars, uSigns);
				}
				double result = Evaluate (first + uSigns [0] + second + uSigns [1] + third + uSigns [2] + fourth);
				Console.WriteLine (result);
				if (result == 24) {
					Console.WriteLine ("Congratulations! You Win!");
				} else if (Math.Abs (result - 24) < 5) {
					Console.WriteLine ("So Close!");
				} else {
					Console.WriteLine ("You lose!");
				}
			} else if (answer == 2) {
				Console.WriteLine ("INSERT DIRECTIONS HERE");
			} else {
				Console.WriteLine ("I'm sorry. I don't understand.");
			}
			return;
		}

		private void SignCheck(int[] vars, string[] uSigns){
			
			string response;
			for (int i = 0; i < 3; i ++) {
				do {
					this.PrintGameLine (vars, uSigns);
					Console.WriteLine ("Would you like to add (+), subtract(-), multiply(*), or divide(/)?");
					response = Console.ReadLine ();
				} while (!signs.Contains (response));

				uSigns [i] = response;
			}
			return;
		}

		private void PrintGameLine(int[] vars, string[] uSigns) {
			Console.WriteLine ("{0} {1} {2} {3} {4} {5} {6}", vars[0], uSigns[0], vars[1], uSigns[1], vars[2], uSigns[2], vars[3]);
		}

		static void ClearLine()
		{
			Console.SetCursorPosition(0,23);
			Console.Write("                                            ");
			Console.SetCursorPosition(0,23);
		}
	}
}