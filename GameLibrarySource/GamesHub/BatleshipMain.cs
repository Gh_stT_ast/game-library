﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;


/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */

///This game was created by students of the Fall 2014 Comp 388-4 course. The game
/// is based on the general rules of the Milton Bradley version of Battleship.
/// All source code was created through original planning and the UI class
/// was supplied by the course instructor.

namespace GamesHub
{
	public class Battleship : IGameCommands
   {
      //The main function handles the title screen, player turns,
      //and initializes ships for both players. Scoring is based on an integer
      //value representing health. An end-game message is dispalyed when either
      //player's health reaches zero.

		public Battleship(){return;}

		public void Help(){
			Console.WriteLine ("Insert instructions here");
		}
		public void Quit() {
			//TODO needs to be implemented
		}
		public void Options() {
			//prints the available actions the user can take
		}

		public void Start()         
      {   

         //Title Screen graphic

         Console.BackgroundColor = ConsoleColor.Black;
         Console.ForegroundColor = ConsoleColor.Blue;
         Console.WriteLine (@"                    ____        __  __  __    _____ __    _     
                   / __ )____ _/ /_/ /_/ /__ / ___// /_  (_)___ ");                          //title screen
         Console.ForegroundColor = ConsoleColor.White;
         Console.WriteLine (@"                  / __  / __ `/ __/ __/ / _ \\__ \/ __ \/ / __ \
                 / /_/ / /_/ / /_/ /_/ /  __/__/ / / / / / /_/ /");
         Console.ForegroundColor = ConsoleColor.Red;
         Console.WriteLine (@"                /_____/\__,_/\__/\__/_/\___/____/_/ /_/_/ .___/ 
                                                       /_/      ");
         Console.ForegroundColor = ConsoleColor.White;
         Console.WriteLine(@"   By: Alysson Almeida, Jarrod Banks, Fabricio Rodrigues, and Krunal Darji



");
         Console.WriteLine (@"               See Battleship Manual in files for instructions
");


         UI.PromptLine (@"                              Press Enter to play");


         //displays for both players using Grid class
         Grid grid1 = new Grid ();                       
         Grid grid2 = new Grid ();

         Console.BackgroundColor = ConsoleColor.Blue;
         Fleet player1Fleet = new Fleet ("Player 1", grid1);   //ship setup for P1
         Console.BackgroundColor = ConsoleColor.Red;
         Fleet player2Fleet = new Fleet ("Player 2", grid2);   //ship setup for P2
        
         int[] p1health = {17};     //initialized health values - 17 total hits to win
         int [] p2health = {17};



         int gameState = 0; //token used to pass turns back and forth

         var P1ShotsTaken = new List<string> ();      
         var P2ShotsTaken = new List<string> ();


         //A loop used to manage player turns. Terminates when either player's score
         //reaches zero.
         while (p1health[0] != 0 && p2health[0] != 0){         
            if (gameState == 0) {
               Console.BackgroundColor = ConsoleColor.DarkBlue;
               P1ShotsTaken = PlayerTurn ("Player 1", player2Fleet, grid2, P1ShotsTaken, p2health);
               gameState = 1;
            }
            else if (gameState == 1) {
               Console.BackgroundColor = ConsoleColor.DarkRed;
               P2ShotsTaken = PlayerTurn ("Player 2", player1Fleet, grid1, P2ShotsTaken, p1health);
               gameState = 0;
            }
         }

         //Displays end-game message based on which player won the game
         if (p2health[0] == 0) {
            Console.WriteLine ("player 1 wins!");
            UI.PromptLine ("Game Over!");
         }
         if (p1health[0] == 0) {
            Console.WriteLine ("Player 2 wins!");
            UI.PromptLine ("Game Over!");
         }
      }



     
      ///This function handles the players' input, checks for hits/misses, and updates a list of shots already taken
      static List<string> PlayerTurn (string player, Fleet fleet, Grid grid, List<string> shotsTaken, int [] playerhealth)
      {
         string shot = "";

         //this loop ensures that a player enters a valid coordinate and checks if it has already been used
         do{

            StartOver: //starting point if the player must enter another shot or re-enter an invalid shot


            //stops the function if the player has no health left
            if (playerhealth[0] == 0) {
               return shotsTaken;
            }

            grid.PrintDisplay(player);
            shot = UI.PromptLine ("Enter coordinate: ");
            shot = shot.ToLower();

            //Checks to see if the shot is a valid answer. Prompts again if invalid.
            do {
               if (!ShotCheck(shot)) {
                  ClearLine();
                  shot = UI.PromptLine("Enter a valid coordinate!: ");
                  shot = shot.ToLower();
               }
            } while (!ShotCheck(shot));



            //Checks if the shot is already taken. If not,
            //Adds the shot to a list of shots already taken.
            //Then checks for hits/misses. Updates the player's grid accordingly.
            //Else if the shot is taken, start over.
            if (!AlreadyShot (shot, shotsTaken)) {
               shotsTaken.Add (shot);
               ClearLine();
               Console.WriteLine("shot fired!");
               Thread.Sleep(500);

               int x = (int)shot[0] - 97;
               int y = (int)shot[1] - 48;
               bool hit = false;
               foreach (Ship s in fleet.GetFleet()) {
                  foreach (int coord in s.GetCoords()) {
                     if (coord == s.GetSea(x,y)) {
                        hit = true;
                        break;
                     }
                  }
               }
               if (hit) {
                  grid.MakeHit(x,y);
                  grid.PrintDisplay(player);
                  ClearLine();
                  Console.WriteLine("That's a hit!");
                  Thread.Sleep(2000);
                  playerhealth[0] = playerhealth[0] - 1;


                  //If the player makes a hit, they return to the beginning and shoot again
                  goto StartOver; //goes to Line 103
               }
               else {
                  grid.MakeMiss(x,y);
                  grid.PrintDisplay(player);
                  ClearLine();
                  Console.WriteLine("You missed!");
               }
            }
            //Shot has already been taken. Start over.
            else {
               Console.SetCursorPosition(0,23);
               Console.WriteLine("You already hit that spot!");
               Thread.Sleep(2000);
               goto StartOver;
            }
         } while (!AlreadyShot (shot, shotsTaken));

         UI.PromptLine("Press Enter for next turn...");

         return shotsTaken;
      }

      ///Checks a users input against a list of previous inputs
      static bool AlreadyShot(string shot, List<string> taken)
      {
         if (taken.Contains (shot)) {
            return true;
         } else {
            return false;
         }
      }


      ///Checks to see if a users input is in the correct format for
      ///further processing. Must have a length of exactly 2.
      ///First character must be between ASCII characters a-j
      ///Second character must be between ASCII characters 0-9
      static bool ShotCheck (string shot)
      {
         if (shot.Length != 2) {
            return false;
         }
         else if ((int)shot[0] < 97 || (int)shot [0] > 106) {
            return false;
         }
         else if ((int)shot [1] < 48 || (int)shot [1] > 57) {
            return false;
         }
         else {
            return true;
         }
      }

      //A graphical function to return the cursor to a specific line
      //to prevent scrolling on the command window.
      static void ClearLine()
      {
         Console.SetCursorPosition(0,23);
         Console.Write("                                            ");
         Console.SetCursorPosition(0,23);
      }
   }
}
