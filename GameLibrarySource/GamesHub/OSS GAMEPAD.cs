﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;


/* This software is protected under the The MIT License (MIT)
 * Copyright 2015 - Copyrightholders: Jarrod Banks, Cher Tam
 */


namespace GamesHub
{
   class MainGame
   {
      public static void Main ()
      {
         Console.WriteLine ("Which game would you like to play?");
         Console.WriteLine ("Press 1 for Battleship");
         Console.WriteLine ("Press 2 for 24");
         int answer = UI.PromptInt (": ");

         if (answer == 1) {
				PlayBattleship ();
         }
         else if (answer == 2) {
            Play24 ();
      }
   }

		private static void PlayBattleship () {
			Battleship game = new Battleship ();
			game.Start();
      }
		private static void PlayBlackjack () {
			Blackjack game = new Blackjack ();
			game.Start();
		}
		private static void Play24 () {
			TwentyFour game = new TwentyFour ();
			game.Start ();
		}
	}
}
